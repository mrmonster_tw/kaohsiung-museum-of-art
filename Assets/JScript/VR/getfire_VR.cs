﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getfire_VR : MonoBehaviour
{
    public GameObject parti;
    public GameObject mn;
    public AudioSource fireparti;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (mn.GetComponent<Main_VR_sun>().sun_firestop)
        {
            parti.SetActive(true);
            if (parti.transform.localScale.x > 0)
            {
                parti.transform.localScale = new Vector3(parti.transform.localScale.x - 0.005f, parti.transform.localScale.y - 0.005f, parti.transform.localScale.z - 0.005f);
                fireparti.volume = fireparti.volume - 0.008f;
            }
            if (parti.transform.localScale.x == 0)
            {
                mn.GetComponent<Main_VR_sun>().sun_firestop = false;
                fireparti.volume = 0;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "match")
        {
            parti.SetActive(true);
        }
    }
}
