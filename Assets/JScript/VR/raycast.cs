﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;

public class raycast : MonoBehaviour
{
    public GameObject canvas;
    public GameObject cam_tra;
    public GameObject cam;
    public GameObject cam_7;
    public GameObject cam_3;
    public GameObject cam_1;
    public GameObject cam_2;
    public GameObject cam_4;
    public GameObject cam_5;
    public GameObject cam_6;
    Vector3 pos;
    public Image circle;
    public bool shot;

    public GameObject pos_1;
    public GameObject pos_2;
    public GameObject pos_3;
    public GameObject pos_4;
    public GameObject pos_5;
    public GameObject pos_6;
    public GameObject pos_7;

    public GameObject painting_1;
    public GameObject painting_2;
    public GameObject painting_3;
    public GameObject painting_4;
    public GameObject painting_5;
    public GameObject painting_6;
    public GameObject painting_7;
    public GameObject 平面;

    public GameObject obj_1;
    public GameObject obj_2;
    public GameObject obj_3;
    public GameObject obj_4;
    public GameObject obj_5;
    public GameObject obj_6;
    public GameObject obj_7;

    public Material paper_a;
    public Material paper_b;
    public Material paper_c;
    public Material paper_d;
    public Material red_paper;
    public Material wave_paper;
    public int check;
    public GameObject book_open;

    public Material 孔廟梁柱;
    public Material 樓梯;
    public Material 橘瓦;
    public Material 石地;
    public Material 紅瓦;
    public Material 門;
    public Material 黑瓦;
    public Material 風景土色A;
    public Material 風景紅牆;
    public Material 風景草;
    public Material 風景藍;
    public Material 風景黃;
    public Material 風景黑牆;

    GameObject obj_7_cn;
    GameObject obj_4_cn;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(loaddev_wait("Cardboard"));
    }

    // Update is called once per frame
    void Update()
    {
        //canvas.transform.localPosition = cam.transform.localPosition;
        //canvas.transform.localEulerAngles = cam.transform.localEulerAngles;
        //pos = cam.transform.TransformPoint(Vector3.forward * 10);
        //Debug.DrawLine(this.transform.position, pos, Color.red, 0.1f, true);

        if (shot)
        {
            Ray ray = new Ray(cam.transform.position, cam.transform.forward * 100);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "painting" || hit.transform.tag == "painting")
                {
                    circle.fillAmount = circle.fillAmount + 0.5f * Time.deltaTime;
                }
                if (hit.transform.tag == "wall")
                {
                    circle.fillAmount = 0;
                }

                if (circle.fillAmount == 1 && hit.transform.name == "paint_1")
                {
                    孔廟梁柱.color = new Color(1,1,1,0);
                    樓梯.color = new Color(1, 1, 1, 0);
                    橘瓦.color = new Color(1, 1, 1, 0);
                    石地.color = new Color(1, 1, 1, 0);
                    紅瓦.color = new Color(1, 1, 1, 0);
                    門.color = new Color(1, 1, 1, 0);
                    黑瓦.color = new Color(1, 1, 1, 0);
                    風景土色A.color = new Color(1, 1, 1, 1);
                    風景紅牆.color = new Color(1, 1, 1, 1);
                    風景草.color = new Color(1, 1, 1, 1);
                    風景藍.color = new Color(1, 1, 1, 1);
                    風景黃.color = new Color(1, 1, 1, 1);
                    風景黑牆.color = new Color(1, 1, 1, 1);

                    //cam_tra.transform.position = pos_1.transform.position;
                    cam.SetActive(false);
                    cam_1.SetActive(true);
                    obj_1.SetActive(true);
                    coll_false(false);

                    shot = false;
                    StartCoroutine(shot_wait(30));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "paint_2")
                {
                    //cam_tra.transform.position = pos_2.transform.position;
                    cam.SetActive(false);
                    cam_2.SetActive(true);
                    obj_2.SetActive(true);
                    if (check == 0)
                    {
                        paper_a.color = new Color(1, 1, 1, 1);
                        paper_b.color = new Color(1, 1, 1, 1);
                        paper_c.color = new Color(1, 1, 1, 1);
                        paper_d.color = new Color(1, 1, 1, 1);
                        red_paper.color = new Color(red_paper.color.r, red_paper.color.g, red_paper.color.b, 1);
                        wave_paper.color = new Color(1, 1, 1, 1);
                        check = 1;
                    }

                    coll_false(false);
                    shot = false;
                    StartCoroutine(shot_wait(25));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "paint_3")
                {
                    //cam_tra.transform.position = pos_3.transform.position;
                    //cam_tra.transform.localEulerAngles = new Vector3(0, -90, 0);
                    obj_3.SetActive(true);
                    StartCoroutine(pic_wait());

                    cam.SetActive(false);
                    cam_3.SetActive(true);

                    coll_false(false);
                    shot = false;
                    StartCoroutine(shot_wait(19));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "paint_4")
                {
                    //cam_tra.transform.position = pos_4.transform.position;
                    cam.SetActive(false);
                    cam_4.SetActive(true);
                    //obj_4.SetActive(true);
                    obj_4_cn = Instantiate(obj_4, painting_4.transform);
                    obj_4_cn.transform.localPosition = new Vector3(0, -0.046f, -8);
                    StartCoroutine(pic_wait());

                    coll_false(false);
                    shot = false;
                    StartCoroutine(shot_wait(35));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "paint_5")
                {
                    //cam_tra.transform.position = pos_5.transform.position;
                    cam.SetActive(false);
                    cam_5.SetActive(true);
                    obj_5.SetActive(true);
                    StartCoroutine(pic_wait());

                    coll_false(false);
                    shot = false;
                    StartCoroutine(shot_wait(34));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "paint_6")
                {
                    //cam_tra.transform.position = pos_6.transform.position;
                    cam.SetActive(false);
                    cam_6.SetActive(true);
                    obj_6.SetActive(true);

                    coll_false(false);
                    shot = false;
                    StartCoroutine(shot_wait(20));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "paint_7")
                {
                    //book_open.SetActive(true);
                    //book_open.GetComponent<Page>().check = 0;
                    //book_open.GetComponent<Page>().fade = false;
                    //book_open.GetComponent<Page>().book_mat.color = new Color(1,1,1,1);
                    //book_open.GetComponent<Page>().book_mat_L.color = new Color(1, 1, 1, 1);
                    //book_open.GetComponent<Page>().book_mat_R.color = new Color(1, 1, 1, 1);
                    //cam_tra.transform.position = pos_7.transform.position;
                    cam.SetActive(false);
                    cam_7.SetActive(true);
                    //obj_7.SetActive(true);
                    obj_7_cn = Instantiate(obj_7);
                    obj_7_cn.transform.localPosition = new Vector3(-32.26f,8,11.4f);
                    //cam_tra.transform.localEulerAngles = new Vector3(0, -90, 0);
                    coll_false(false);
                    shot = false;
                    StartCoroutine(shot_wait(35));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "平面")
                {
                    hit.transform.GetComponent<BoxCollider>().enabled = false;
                    shot = false;
                    circle.fillAmount = 0;
                    check = 0;
                    Application.LoadLevel("Home");
                }
            }
        }
        else
        {
            circle.fillAmount = 0;
            check = 0;
        }
    }
    IEnumerator shot_wait(int xx)
    {
        yield return new WaitForSeconds(2);
        shot = true;
        yield return new WaitForSeconds(xx);
        Destroy(obj_7_cn);
        Destroy(obj_4_cn);
        //cam_tra.transform.position = new Vector3(-21.4f,7.5f,12.6f);
        //cam_tra.transform.localEulerAngles = new Vector3(0,0,0);
        cam.SetActive(true);
        cam_1.SetActive(false);
        cam_2.SetActive(false);
        cam_3.SetActive(false);
        cam_4.SetActive(false);
        cam_5.SetActive(false);
        cam_6.SetActive(false);
        cam_7.SetActive(false);

        coll_false(true);
        obj_1.SetActive(false);
        obj_2.SetActive(false);
        obj_3.SetActive(false);
        obj_5.SetActive(false);
        obj_6.SetActive(false);
        obj_5.transform.localPosition = new Vector3(0, 0, 500);
        obj_4.transform.localPosition = new Vector3(0, 0, 500);
        obj_3.transform.localPosition = new Vector3(0, 0, 500);
    }
    IEnumerator pic_wait()
    {
        yield return new WaitForSeconds(1f);
        obj_5.transform.localPosition = new Vector3(obj_5.transform.localPosition.x, obj_5.transform.localPosition.y, -24);
        obj_4.transform.localPosition = new Vector3(obj_4.transform.localPosition.x, obj_4.transform.localPosition.y, -24);
        obj_3.transform.localPosition = new Vector3(obj_3.transform.localPosition.x, obj_3.transform.localPosition.y, 34);
    }
    IEnumerator loaddev_wait(string newdev)
    {
        XRSettings.LoadDeviceByName(newdev);
        yield return null;
        XRSettings.enabled = true;
    }
    void coll_false(bool xx)
    {
        painting_1.GetComponent<BoxCollider>().enabled = xx;
        painting_2.GetComponent<BoxCollider>().enabled = xx;
        painting_3.GetComponent<BoxCollider>().enabled = xx;
        painting_4.GetComponent<BoxCollider>().enabled = xx;
        painting_5.GetComponent<BoxCollider>().enabled = xx;
        painting_6.GetComponent<BoxCollider>().enabled = xx;
        painting_7.GetComponent<BoxCollider>().enabled = xx;
        平面.GetComponent<BoxCollider>().enabled = xx;
    }
}
