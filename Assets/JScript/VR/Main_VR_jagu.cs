﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.Playables;
using UnityEngine.XR;
using UnityEngine.UI;

public class Main_VR_jagu : MonoBehaviour
{
    public GameObject jagu_closebook;
    public GameObject jagu_1st;
    public GameObject jagu_2st;
    public bool jagu_touch;

    public Material jagu_paper;
    public Material book_mat;
    public Material book_mat_L;
    public Material book_mat_R;

    public int jagu_number;

    GameObject jagu_pack_cn;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //甲骨文
        if (jagu_number == 0)
        {
            book_mat.color = new Color(1, 1, 1, 1);
            book_mat_L.color = new Color(1, 1, 1, 1);
            book_mat_R.color = new Color(1, 1, 1, 1);
            jagu_paper.SetFloat("_Threshold", 0);
            jagu_paper.SetFloat("_EdgeLength", 0);

            jagu_number = 1;
            StartCoroutine(ani_1_fn());
        }
    }
    IEnumerator ani_1_fn()
    {
        yield return new WaitForSeconds(15f);
        jagu_1st.GetComponent<PlayableDirector>().enabled = false;
        jagu_2st.GetComponent<PlayableDirector>().enabled = true;
        jagu_closebook.SetActive(false);
    }
}
