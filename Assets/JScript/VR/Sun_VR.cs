﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Video;

public class Sun_VR : MonoBehaviour
{
    public GameObject sun_matches;
    public GameObject plane;
    public int check;
    public GameObject mn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (mn.GetComponent<Main_VR_sun>().sun_firestop == true && check == 0)
        {
            sun_matches.SetActive(false);
            plane.GetComponent<VideoPlayer>().enabled = true;
            plane.GetComponent<PlayableDirector>().enabled = true;
            StartCoroutine(wait_show());
            check = 1;
        }
    }
    IEnumerator wait_show()
    {

        yield return new WaitForSeconds(2f);
        plane.transform.localPosition = new Vector3(0, 0, 0);
    }
}
