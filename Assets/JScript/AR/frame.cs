﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class frame : MonoBehaviour
{
    public bool swit;

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<SpriteRenderer>().material.color.a > 0 && swit == false)
        {
            this.GetComponent<SpriteRenderer>().material.color = new Color(this.GetComponent<SpriteRenderer>().material.color.r, this.GetComponent<SpriteRenderer>().material.color.g, this.GetComponent<SpriteRenderer>().material.color.b, this.GetComponent<SpriteRenderer>().material.color.a - 0.02f);
        }
        if (this.GetComponent<SpriteRenderer>().material.color.a <= 0)
        {
            swit = true;
        }
        if (this.GetComponent<SpriteRenderer>().material.color.a < 1 && swit == true)
        {
            this.GetComponent<SpriteRenderer>().material.color = new Color(this.GetComponent<SpriteRenderer>().material.color.r, this.GetComponent<SpriteRenderer>().material.color.g, this.GetComponent<SpriteRenderer>().material.color.b, this.GetComponent<SpriteRenderer>().material.color.a + 0.02f);
        }
        if (this.GetComponent<SpriteRenderer>().material.color.a >= 1)
        {
            swit = false;
        }
    }
}
