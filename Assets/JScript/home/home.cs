﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class home : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(loaddev_wait("None"));
    }
    public void chose(int xx)
    {
        if (xx == 0)
        {
            //StartCoroutine(loaddev_wait("None"));
            Application.LoadLevel("AR");
        }
        if (xx == 1)
        {
            //StartCoroutine(loaddev_wait("Cardboard"));
            Application.LoadLevel("VR");
        }
        if (xx == 2)
        {
            Application.Quit();
        }
    }
    IEnumerator loaddev_wait(string newdev)
    {
        XRSettings.LoadDeviceByName(newdev);
        yield return null;
        XRSettings.enabled = false;
    }
}
